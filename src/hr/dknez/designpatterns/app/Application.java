package hr.dknez.designpatterns.app;

import hr.dknez.designpatterns.behavioral.interpreter.InterpreterDemo;

public class Application {

   public static void main(String args[]) {
      //      DbSingletonDemo.runDbSingletonDemo();
      //      LunchOrderBeanDemo.runLunchOrderBeanDemo();
      //      LunchOrderBuilderDemo.runLunchBuilderDemo();
      //      PrototypeDemo.runPrototypeDemo();
      //      FactoryDemo.runFactoryDemo();
      //      AbstractFactoryDemo.runAbstractFactoryDemo();
      //      AdapterDemo.runAdapterDemo();
      //      Shape2BridgeDemo.runShape2BridgeDemo();
      //      MovieBridgeDemo.runMovieBridgeDemo();
      //      CompositeEverydayDemo.runComopsiteEverydayDemo();
      //      CompositeMenuDemo.runCompositeMenuDemo();
      //      DecoratorSandwichDemo.runDecoratorSandwichDemo();
      //      FacadeJdbcDemo.runFacadeJdbcDemo();
      //      FlyweightInventoryDemo.runFlyweightInventoryDemo();
      //      TwitterDemo.runTwitterDemo();
      //      ChainOfResponsibilityDemo.runChainOfResponsibilityDemo();
      //      CommandDemo.runCommandDemo();
      InterpreterDemo.runInterpreterDemo();
   }
}
