package hr.dknez.designpatterns.structural.adapter;

import java.util.List;

public class AdapterDemo {

   public static void runAdapterDemo() {
      EmployeeClient client = new EmployeeClient();

      List<Employee> employees = client.getEmployeeList();

      System.out.println(employees);
   }
}
