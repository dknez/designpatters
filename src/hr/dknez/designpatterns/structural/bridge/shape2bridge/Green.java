package hr.dknez.designpatterns.structural.bridge.shape2bridge;

public class Green implements Color {

   @Override
   public void applyColor() {
      System.out.println("Applying green color");
   }
}
