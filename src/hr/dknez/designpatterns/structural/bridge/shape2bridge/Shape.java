package hr.dknez.designpatterns.structural.bridge.shape2bridge;

public abstract class Shape {

   protected Color color;

   public Shape(Color color) {
      this.color = color;
   }

   abstract public void applyColor();
}
