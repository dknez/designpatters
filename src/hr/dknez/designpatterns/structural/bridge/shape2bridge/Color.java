package hr.dknez.designpatterns.structural.bridge.shape2bridge;

public interface Color {

   public void applyColor();
}
