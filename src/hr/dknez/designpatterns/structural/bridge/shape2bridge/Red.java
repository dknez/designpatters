package hr.dknez.designpatterns.structural.bridge.shape2bridge;

public class Red implements Color {

   @Override
   public void applyColor() {
      System.out.println("Applying red color");
   }
}
