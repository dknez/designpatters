package hr.dknez.designpatterns.structural.bridge.shape2bridge;

public class Square extends Shape {

   public Square(Color color) {
      super(color);
   }

   @Override
   public void applyColor() {
      color.applyColor();
   }
}
