package hr.dknez.designpatterns.structural.bridge.shape2bridge;

public class Blue implements Color {

   @Override
   public void applyColor() {
      System.out.println("Applying blue color");
   }
}
