package hr.dknez.designpatterns.structural.bridge.moviebridge;

import java.util.List;

public interface Formatter {
   String format(String header, List<Detail> details);
}
