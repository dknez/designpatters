package hr.dknez.designpatterns.structural.facade;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbSingleton {

   private static volatile DbSingleton instance = null;

   private Connection conn = null;

   private DbSingleton() {
      if (instance != null) {
         throw new RuntimeException("Use getInstance() method to create");
      }
   }

   public static DbSingleton getInstance() {
      if (instance == null) {
         synchronized (DbSingleton.class) {
            if (instance == null) {
               instance = new DbSingleton();
            }
         }
      }
      return instance;
   }

   public Connection getConnection() throws SQLException {
      if (conn == null || conn.isClosed()) {
         synchronized (DbSingleton.class) {
            if (conn == null || conn.isClosed()) {
               try {
                  String dbUrl = "jdbc:derby:memory:codejava/webdb;create=true";

                  conn = DriverManager.getConnection(dbUrl);
               } catch (SQLException e) {
                  e.printStackTrace();
               }
            }
         }
      }

      return conn;
   }
}
