package hr.dknez.designpatterns.structural.proxy;

public class TwitterDemo {

   public static void runTwitterDemo() {
      TwitterService service = (TwitterService) SecurityProxy.newInstance(new TwitterServiceStub());

      System.out.println(service.getTimeline("whatever value"));
   }
}
