package hr.dknez.designpatterns.structural.decorator;

public class DecoratorSandwichDemo {

   public static void runDecoratorSandwichDemo() {
      Sandwich sandwich = new DressingDecorator(new MeatDecorator(new SimpleSandwich()));

      System.out.println(sandwich.make());
   }
}
