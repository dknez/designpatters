package hr.dknez.designpatterns.structural.decorator;

public interface Sandwich {

    public String make();
}
