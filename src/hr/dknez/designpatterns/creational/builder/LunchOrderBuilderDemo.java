package hr.dknez.designpatterns.creational.builder;

public class LunchOrderBuilderDemo {

   public static void runLunchBuilderDemo() {
      LunchOrder.Builder builder = new LunchOrder.Builder();

      builder.bread("Wheat")
            .condiments("Lettuce")
            .dressing("Mayo")
            .meat("Ham");

      LunchOrder lunchOrder = builder.build();

      System.out.println(lunchOrder.getBread());
      System.out.println(lunchOrder.getCondiments());
      System.out.println(lunchOrder.getDressing());
      System.out.println(lunchOrder.getMeat());
   }
}
