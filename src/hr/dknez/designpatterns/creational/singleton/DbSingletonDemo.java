package hr.dknez.designpatterns.creational.singleton;

public class DbSingletonDemo {

   public static void runDbSingletonDemo() {
      DbSingleton instance = DbSingleton.getInstance();

      System.out.println(instance);

      DbSingleton anotherInstance = DbSingleton.getInstance();

      System.out.println(anotherInstance);
   }
}
