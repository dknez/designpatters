package hr.dknez.designpatterns.creational.factory;

public enum WebsiteType {
   BLOG,
   SHOP
}
