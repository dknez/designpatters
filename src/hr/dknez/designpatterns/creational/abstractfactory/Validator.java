package hr.dknez.designpatterns.creational.abstractfactory;

public interface Validator {
   public boolean isValid(CreditCard creditCard);
}
