package hr.dknez.designpatterns.creational.abstractfactory;

public class CreditCard {

   protected int carNumberLength;

   protected int cscNumber;

   public int getCarNumberLength() {
      return carNumberLength;
   }

   public void setCarNumberLength(int carNumberLength) {
      this.carNumberLength = carNumberLength;
   }

   public int getCscNumber() {
      return cscNumber;
   }

   public void setCscNumber(int cscNumber) {
      this.cscNumber = cscNumber;
   }
}
