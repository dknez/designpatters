package hr.dknez.designpatterns.creational.abstractfactory;

public enum CardType {
   GOLD,
   PLATINUM
}
