package hr.dknez.designpatterns.behavioral.interpreter;

public interface Expression {

   public boolean interpret(String context);
}
