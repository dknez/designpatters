package hr.dknez.designpatterns.behavioral.chainofresponsibility;

public class Director extends Handler {

   @Override
   public void handleRequest(Request request) {
      if (request.getRequestType() == RequestType.CONFERENCE) {
         System.out.println("Directors can approve conferences.");
      } else {
         System.out.println("VP cannot handle this request. Calling other member in chain.");
         successor.handleRequest(request);
      }
   }
}
