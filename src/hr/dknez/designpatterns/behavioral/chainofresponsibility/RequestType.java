package hr.dknez.designpatterns.behavioral.chainofresponsibility;

public enum RequestType {
   CONFERENCE, PURCHASE
}
