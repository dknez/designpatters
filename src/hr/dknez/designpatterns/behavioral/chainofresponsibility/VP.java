package hr.dknez.designpatterns.behavioral.chainofresponsibility;

public class VP extends Handler {


   @Override
   public void handleRequest(Request request) {
      if (request.getRequestType() == RequestType.PURCHASE) {
         if (request.getAmount() < 1500) {
            System.out.println("VPs can approve purchases below 1500");
         } else {
            System.out.println("VP cannot handle this request. Calling other member in chain.");
            successor.handleRequest(request);
         }
      }
   }
}
